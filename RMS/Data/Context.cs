﻿using RMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RMS.Data
{
    public class Context:DbContext
    {
        public DbSet<User> userDB { get; set; }
        public DbSet<Item> itemDB { get; set; }
        public DbSet<Admin> adminDB { get; set; }
        public DbSet<Cart> cartDB { get; set; }
        public DbSet<Order> orderDB { get; set; }


        public System.Data.Entity.DbSet<RMS.Models.PaymentDetails> PaymentDetails { get; set; }
    }
}