﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Models
{
    public class Admin
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "User Name")]
        public string userName { get; set; }


        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public String password { get; set; }

        [NotMapped]
        [Display(Name = "Remember Me")]
        public bool remeberMe { get; set; }
    }
}