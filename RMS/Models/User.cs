﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Models
{
    public class User
    {

        [Display(Name = "First Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter Last Name")]
        public string lastName { get; set; }


        [Display(Name = "Age")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter age")]
        public int age { get; set; }


        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter email address")]
        public string email { get; set; }


        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "User Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter User Name")]
        public string userName { get; set; }


        [Display(Name ="Password")]
        [DataType(DataType.Password)]
        [MinLength(4, ErrorMessage ="Password should contain minimum 4 digits")]
        public String password { get; set; }



        [NotMapped]
        [Display(Name = "Confirm Password")]
        [MinLength(4, ErrorMessage = "Password should contain minimum 4 digits")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage ="Password not match. Please try again")]
        public String confirmPassword { get; set; }



        [NotMapped]
        [Display(Name = "Remember Me")]
        public bool remeberMe { get; set; }




    }
}