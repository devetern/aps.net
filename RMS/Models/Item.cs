﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int itemId { get; set; }

        [Display(Name = "Item Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Item Name")]
        public string itemName { get; set; }

        [Display(Name = "Item Price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Item Price")]
        public double itemPrice { get; set; }


        [Display(Name = "Item Description")]
        public string itemDescription { get; set; }


        [Display(Name = "Item Quentity")]
        public int itemQnt { get; set; }


        [Display(Name = "Upload image")]
        public string itemImage { get; set; }

        [NotMapped]
        public HttpPostedFileBase imageFile { get; set; }
    }
}