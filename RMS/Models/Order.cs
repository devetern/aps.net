﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int orderId { get; set; }


        [Display(Name = "Customer Name")]
        public string customerName { get; set; }


        [Display(Name = "Customer address")]
        public string CustomerAddress { get; set; }


        [Display(Name = "User Name")]
        public string userName { get; set; }


    }
}