﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Models
{
    public static class UserSession
    {
        public static string TestSessionValue
        {
            get
            {
                object value = HttpContext.Current.Session["TestSessionValue"];
                return value == null ? "" : (string)value;
            }
            set
            {
                HttpContext.Current.Session["TestSessionValue"] = value;
            }
        }
    }
}