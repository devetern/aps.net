namespace RMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RM_DB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        userName = c.String(nullable: false, maxLength: 128),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.userName);
            
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        itemId = c.String(),
                        userID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        itemId = c.Int(nullable: false, identity: true),
                        itemName = c.String(nullable: false),
                        itemPrice = c.Double(nullable: false),
                        itemDescription = c.String(),
                        itemQnt = c.Int(nullable: false),
                        itemImage = c.String(),
                    })
                .PrimaryKey(t => t.itemId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        orderId = c.Int(nullable: false, identity: true),
                        customerName = c.String(),
                        CustomerAddress = c.String(),
                        userName = c.String(),
                    })
                .PrimaryKey(t => t.orderId);
            
            CreateTable(
                "dbo.PaymentDetails",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        customerName = c.String(),
                        CustomerAddress = c.String(),
                        userName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        userName = c.String(nullable: false, maxLength: 128),
                        firstName = c.String(nullable: false),
                        lastName = c.String(nullable: false),
                        age = c.Int(nullable: false),
                        email = c.String(nullable: false),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.userName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.PaymentDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.Items");
            DropTable("dbo.Carts");
            DropTable("dbo.Admins");
        }
    }
}
