﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RMS.Data;
using RMS.Models;

namespace RMS.Controllers
{
    public class PaymentDetailsController : Controller
    {
        private Context db = new Context();

        // GET: PaymentDetails
        public ActionResult Index()
        {
            string id = (string)Session["userName"];
            var ob = db.PaymentDetails.SqlQuery("select * from PaymentDetails where userName=@id", new SqlParameter("@id", id));
            return View(ob.ToList());
        }

        // GET: PaymentDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDetails paymentDetails = db.PaymentDetails.Find(id);
            if (paymentDetails == null)
            {
                return HttpNotFound();
            }
            return View(paymentDetails);
        }

        // GET: PaymentDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PaymentDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PaymentDetails paymentDetails)
        {
            if (ModelState.IsValid)
            {
                Session["cuName"] = paymentDetails.customerName;
                Session["cuAddress"] = paymentDetails.CustomerAddress;

                string useName = (string)Session["userName"];

                paymentDetails.userName = useName;

                db.PaymentDetails.Add(paymentDetails);
                db.SaveChanges();

                
                return RedirectToAction("Index");
            }

            return View(paymentDetails);
        }

        // GET: PaymentDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDetails paymentDetails = db.PaymentDetails.Find(id);
            if (paymentDetails == null)
            {
                return HttpNotFound();
            }
            return View(paymentDetails);
        }

        // POST: PaymentDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,customerName,CustomerAddress")] PaymentDetails paymentDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paymentDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paymentDetails);
        }

        // GET: PaymentDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentDetails paymentDetails = db.PaymentDetails.Find(id);
            if (paymentDetails == null)
            {
                return HttpNotFound();
            }
            return View(paymentDetails);
        }

        // POST: PaymentDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PaymentDetails paymentDetails = db.PaymentDetails.Find(id);
            db.PaymentDetails.Remove(paymentDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        //generate Order
        public ActionResult GenarateOrders(int ? id)
        {
            if (Request.IsAuthenticated)
            {
                PaymentDetails ob = db.PaymentDetails.Find(id);

                string userName = (string)Session["userName"];
                Session["cuName"] = ob.customerName;
                Session["cuAddress"] = ob.CustomerAddress;

                Order ob2 = new Order();
                ob2.customerName= ob.customerName; ;
                ob2.CustomerAddress= ob.CustomerAddress;
                ob2.userName = userName;

                db.orderDB.Add(ob2);
                db.SaveChanges();

                return View();
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }

        }

    }
}
