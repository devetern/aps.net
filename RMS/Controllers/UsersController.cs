﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using RMS.Data;
using RMS.Models;

namespace RMS.Controllers
{
    public class UsersController : Controller
    {
        private Context db = new Context();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.userDB.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details()
        {
            if (Request.IsAuthenticated)
            {
                string id = (string)Session["userName"];
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.userDB.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user); 
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }


        }

        public ActionResult DetailsItem(int id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item user = db.itemDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);



        }


        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.userDB.Add(user);
                db.SaveChanges();
                return RedirectToAction("UserLogin");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit()
        {
            string id = (string)Session["userName"];

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.userDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            string id = (string)Session["userName"];
            user.userName = id;
            
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UserHome");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete()
        {
            string id = (string)Session["userName"];
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.userDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed()
        {
            string id = (string)Session["userName"];
            User user = db.userDB.Find(id);
            db.userDB.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult UserLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserLogin(User ob)
        {
            if (userValidate(ob.userName, ob.password) == true)
            {
                FormsAuthentication.SetAuthCookie(ob.userName, ob.remeberMe ? true : false);
                Session["userName"] = ob.userName;
              
                return RedirectToAction("UserHome", "Users");
            }
            return View(ob);
        }

        

        //logout for admin
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("UserLogin", "Users"); ;
        }

        //user validating method
        public bool userValidate(String un, String pw)
        {
            bool uState = false;
            int uCont = db.userDB.Count(x => x.userName == un && x.password == pw);
            if (uCont == 1)
            {
                uState = true;
            }
            return uState;
        }

        //list of users
        public ActionResult UserHome()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.itemDB.ToList());
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }
            
        }

        //Admin HomePage
        public ActionResult AddtoCart(int id)
        {
            
            

            string userName = (string)Session["userName"];
            int iCont = db.cartDB.Count(x => x.itemId ==id.ToString() && x.userID == userName);



            if (iCont!=1)
            {
                Cart ob = new Cart();
                ob.itemId = id.ToString();
                ob.userID = (string)Session["userName"];

                db.cartDB.Add(ob);
                db.SaveChanges();
                
            }
        
            return RedirectToAction("UserHome");

        }

        //UserProfile
        public ActionResult UserProfile()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Details", "Users");
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }

        }

        //DisplayCart
        public ActionResult DisplayCart()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Carts");
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }

        }

        //get order data
        public ActionResult GetOrderData()
        {
            if (Request.IsAuthenticated)
            {
                string userName = (string)Session["userName"];
                string cuName= (string)Session["cuName"];
                string cuAddress = (string)Session["cuAddress"];

                Cart card = db.cartDB.Find(userName);
                return RedirectToAction("Index", "Carts");
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }

        }

        //generate Order
        public ActionResult GenarateOrder(int id)
        {
            if (Request.IsAuthenticated)
            {
                PaymentDetails ob = db.PaymentDetails.Find(id);

                string userName = (string)Session["userName"];
                Session["cuName"] = ob.customerName;
                Session["cuAddress"] = ob.CustomerAddress;

                db.PaymentDetails.Add(ob);
                db.SaveChanges();



                //Cart card = db.cartDB.Find(userName);

                return View();
            }
            else
            {
                return RedirectToAction("UserLogin", "Users");
            }

        }

    }
}
