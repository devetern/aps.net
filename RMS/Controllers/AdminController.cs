﻿using RMS.Data;
using RMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RMS.Controllers
{
    public class AdminController : Controller
    {
        private Context db = new Context();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        

        [HttpPost]
        public ActionResult Index(Admin ob)
        {
            if (userValidate(ob.userName, ob.password) == true)
            {
                FormsAuthentication.SetAuthCookie(ob.userName, ob.remeberMe ? true : false);
                Session["Ädmin"] = ob.userName;
                return RedirectToAction("AdminHome", "Admin");
            }
            return View(ob);
        }


        //logout for admin
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Customers"); ;
        }

        //user validating method
        public bool userValidate(String un, String pw)
        {
            bool uState = false;
            int uCont = db.adminDB.Count(x => x.userName == un && x.password == pw);
            if (uCont == 1)
            {
                uState = true;
            }
            return uState;
        }

        //list of users
        public ActionResult Home()
        {
            return View(db.userDB.ToList());
        }

        //Admin HomePage
        public ActionResult AdminHome()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.itemDB.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.userDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }


        // GET: Users/Details/5
        public ActionResult DetailsItem(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.itemDB.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.userDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "userName,password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.userDB.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            User user = db.userDB.Find(id);
            db.userDB.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        // GET: GenerateReports
        public ActionResult ListOfCustomer()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.userDB.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
            
        }

        //GET: ListOfItems
        public ActionResult ListOfItems()
        {
            if (Request.IsAuthenticated)
            {
                return View(db.itemDB.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
           
        }

        //GET: ListOfOreders
        public ActionResult ListOfOreders()
        {

            return View(db.orderDB.ToList());
        }


        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.userDB.Add(user);
                db.SaveChanges();
                return RedirectToAction("Home");
            }

            return View(user);
        }




    }
}